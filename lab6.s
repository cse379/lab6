	AREA interrupts, CODE, READWRITE
	EXPORT lab6
	EXPORT FIQ_Handler
	EXTERN uart_init
	EXTERN read_character
	EXTERN pin_connect_block_setup_for_uart0
	EXTERN output_character
	EXTERN output_string
	EXTERN enter
	EXTERN div_and_mod
	EXTERN make_chars_2
	

prompt = "\n\r\n\r\t\tWelcome to lab #6\n\r\n\r",0
	ALIGN	
instructions = "This program lets you interact with a moving character in a box\n\rof dimension 15 by 15 characters.  The character is randomly selected\n\rfrom *, @, #, and X, and is initially placed at a random position in the box\n\rwith random initial movement direction in either the UP, DOWN, LEFT or RIGHT\n\rdirection with speed of one position per half-second.  A count of the number of\n\rWalls Encountered is displayed at the top of the gameboard.  Every time the\n\rcharacter encounters the boundary of the box, it bounces off, starts moving in\n\rthe opposite direction, and the Walls Encountered count will increase by one.\n\r\n\rPress 'Space' to pause the game. Press 'Space' again to resume the game.\n\rPress the USER INTERRUPT BUTTON on the board (P0.14) to place a new random\n\rcharacter at center of gameboard with random movement direction.\n\rPress 'w' to set direction of character to UP.\n\rPress 'a' to set direction of character to LEFT.\n\rPress 's' to set direction of character to DOWN.\n\rPress 'd' to set direction of character to RIGHT.\n\rPress '+' to double speed of character.\n\rPress '-' to halve speed of character.\n\rPress 'q' at ANY time to END Program.\n\r\n\r--- Press 'ENTER' to start game... ---",0
	ALIGN
walls_enc = "Walls Encountered: ",0
	ALIGN
screen_padding = "   ",0
	ALIGN
quit_prompt = "\n\rYou have exited the program.",0
	ALIGN
q_PRESSED = "\0",0     ; 1 means user has pressed 'q' to quit program, 0 means they have not. Initialized to 0.
	ALIGN
ENTER_PRESSED = "\0",0 ; 1 means user has pressed 'ENTER' to begin game, 0 means they have not. Initialized t0 0.
	ALIGN
PAUSED = "\0",0		   ; 1 means game is paused, 0 means game is active; initialized to 0
	ALIGN
SCORE = "\0\0\0\0",0	; Number of walls character has hit, initialized to 0
	ALIGN
SCORE_STRING = "\0\0\0\0\0\0\0\0",0		; ASCII String corresponding to SCORE variable
	ALIGN
char_identity = "\0",0		; identity of character being displayed to string; has ASCII value of corresponding character (e.g., 0x2A for '*')
	ALIGN
char_direction = "\0\0\0\0",0		; direction of movement of character
	ALIGN
char_position = "\0\0\0\0",0 ; current position of character. Equal to byte position in "string array" gb_0 thru gb_16. 
	ALIGN
							; Top left position: 0x13 (19)
							; Top right position: 0x21 (33)
							; Center position: 0x98 (152)
							; Botton Left: 0x10F (271)
							; Botton Right: 0x11D (285)

; Wall position variables (booleans)
AT_TOP_WALL = "\0",0	; 1 == at top wall, 0 not   position in range: [19,33]
	ALIGN
AT_LEFT_WALL = "\0",0	; 1 == at left wall, 0 not  (char_position - 1) % 18 == 0
	ALIGN
AT_RIGHT_WALL = "\0",0  ; 1 == at right wall, 0 not	(char_position + 3) % 18 == 0 
	ALIGN
AT_BOTTOM_WALL = "\0",0 ; 1 == at bottom wall, 0 not	position in range: [271, 285]
	ALIGN
FORM_FEED = "\f",0
	ALIGN
plus_PRESSED = "\0",0	; 1 if user has pressed '+'; reset to 0 after handling action associated with this button press
	ALIGN
minus_PRESSED = "\0",0	; 1 if user has pressed '-'; reset to 0 after handling action associated with this button press
	ALIGN

; MOVEMENT DIRECTIONS that char_direction can take on
; We update char_position to be char_position := char_position + char_direction every time Timer0 interrupt occurs
UP EQU 0xFFFFFFEE			; UP is -18 (going one position up on game board)
DOWN EQU 0x00000012			; DOWN is +18 (going one position down on game board)
LEFT EQU 0xFFFFFFFF			; LEFT is -1 (going one position left on game board)
RIGHT EQU 0x00000001		; RIGHT is +1 (going one position right on game board)

GB_WIDTH EQU 0x12 	       ; Gameboard is 18 (0x12 == 18) bytes wide
GB_CENTER EQU 0x98	       ; Gameboard center is btye 152 (0x98 == 152) starting at address gb_0
ASTERISK_ASCII EQU 0x2A	   ; '*' symbol ASCII
AT_ASCII EQU 0x40		   ; '@' symbol ASCII
HASH_ASCII EQU 0x23		   ; '#' symbol ASCII
X_ASCII EQU 0x58           ; 'X' symbol ASCII
	
; Timer Counter (TC) Register holds the count
T0TC EQU 0xE0004008		   ; Timer0 Address
T1TC EQU 0xE0008008		   ; Timer1 Address
	
T0MCR EQU 0xE0004014	   ; Timer0 Match Control Register
T1MCR EQU 0xE0008014       ; Timer1 Match Control Register
	
MR1 EQU 0xE000401C		   ; Match Register 1 (Determines timeout period of Timer0 -- when interrupt is triggered. T0TC is reset upon interrupt.)

; Interrupt Registers
; Detecting Pending Interrupt: examine to determine if interrupt pending; Bit 1 is set if an interrupt is pending du to Match Register 1 (MR1) matching the Timer Count Register (TC)
; Clearing the Interrupt: If interrupt was due to MR1 matching TC, write a 1 to bit 1 in IR to clear interrupt.
T0IR EQU 0xE0004000		   ; Timer 0 Interrupt Register
T1IR EQU 0xE0008000		   ; Timer 1 Interrupt Register

; Timer Control Registers (TCR)
; Used to Enable / Disable Timer(s) and to Reset Timer Count Register (TC) to 0
; Bit 0: Set to 1 to Enable; Set to 0 to Disable
; Bit 1: Set to 1 to Reset Timer Count Register (TC) to 0
T0TCR EQU 0xE0004004
T1TCR EQU 0xE0008004
	

	
TIMEOUT_PERIOD = "\0\0\0\0",0		; Number of "ticks" after which Timer0 will trigger interrupt.
									; Initially equals 0x008CA000 (9216000), the number of ticks in 0.5 seconds at 18.432 MHz (14.7456 * (5/4) MHz)
	ALIGN
	
TIMEOUT_PERIOD_INIT EQU 0x008CA000

; strings corresponding to gameboard
; print_gameboard prints these along with the score and form feed

gb_0 =  "|---------------|\0"
gb_1 =  "|               |\0"
gb_2 =  "|               |\0"
gb_3 =  "|               |\0"
gb_4 =  "|               |\0"
gb_5 =  "|               |\0"
gb_6 =  "|               |\0"
gb_7 =  "|               |\0"
gb_8 =  "|               |\0"
gb_9 =  "|               |\0"
gb_10 = "|               |\0"
gb_11 = "|               |\0"
gb_12 = "|               |\0"
gb_13 = "|               |\0"
gb_14 = "|               |\0"
gb_15 = "|               |\0"
gb_16 = "|---------------|\0"

	ALIGN

lab6	 		STMFD sp!, {lr}

				; Call our setup functions
				BL pin_connect_block_setup_for_uart0
				BL uart_init
				BL interrupt_init
				
				; Enable Timer1 (Used for random-number generation)
				LDR r4, =T1TCR
				LDR r1, [r4]
				ORR r1, r1, #0x01
				STR r1, [r4]
				
				; Display prompt / welcome message
				LDR r4, =prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Display user instructions
				LDR r4, =instructions
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				BL enter
				BL enter

				; We stay in this loop until the user presses 'q' to QUIT to program				
loop			LDR r0, =q_PRESSED	; Load q_PRESSED address
				LDRB r1, [r0]		; Load q_PRESSED memory contents (single byte)
				CMP r1, #1          ; Compare contents to 1
				BEQ	quit            ; If contents == 1, goto quit
				B loop              ; Else loop again and check if 'q' was pressed
				
quit			BL disable_interrupts	; If user has QUIT the program, we need to disable all further interrupts
				
				; Display exit message
				LDR r4, =quit_prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Return to caller
				LDMFD sp!,{lr}
				BX lr
				
; print_gameboard prints strings
; gb_0 ... gb_16 consecutively with a carriage return
; and new line printed in between each string.
; This displays the gameboard in PuTTY.
; screen_padding string is used to add three spaces in front of each gb_x string
; Number of walls encountered + SCORE_STRING are printed above gameboard
; Numeric score is converted to string by calling stringify_SCORE
print_gameboard
				STMFD SP!, {r0-r12, lr}
				
				; Print new page in PuTTY
				LDR r4, =FORM_FEED
				BL output_string
				
				; Numeric score is converted to string
				BL stringify_SCORE
				
				; Print "Walls Encountered: "
				LDR r4, =walls_enc
				BL output_string
				; Print string equivalent of numeric SCORE (format: 009, etc.)
				LDR r4, =SCORE_STRING
				BL output_string
				BL enter
				
				; screen_padding string is used to add three spaces in front of each gb_x string
				LDR r4, =screen_padding
				BL output_string
				; Print string gb_0, the top row of gameboard (border)
				LDR r4, =gb_0
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_1
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_2
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_3
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_4
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_5
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_6
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_7
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_8
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_9
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_10
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_11
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_12
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_13
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_14
				BL output_string
				BL enter
				
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_15
				BL output_string
				BL enter
				
				; Print bottom border of gameboard
				LDR r4, =screen_padding
				BL output_string
				LDR r4, =gb_16
				BL output_string
				BL enter
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return


interrupt_init       
				STMFD SP!, {r0-r1, lr}   ; Save registers 
				
				; Push button setup		 
				LDR r0, =0xE002C000
				LDR r1, [r0]
				ORR r1, r1, #0x20000000
				BIC r1, r1, #0x10000000
				STR r1, [r0]  ; PINSEL0 bits 29:28 = 10

				; Classify sources as IRQ or FIQ
				; 1 == FIQ, 0 == IRQ
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0xC]
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 interrupt
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0xC]

				; Enable Interrupts
				; 1 == enable, 0 == NO EFFECT
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x10] 
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt enable 
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x10]

				;UART0 Interrupt When Data Recieved U0IER
				LDR r0, =0xE000C004		; Load U0EIR address
				LDR r1, [r0]			; Load U0EIR contents
				ORR r1, r1, #1			; Set bit 0 to 1 to enable Data recieved interrupt
				STR r1, [r0]			; Store contents back

				; External Interrupt 1 setup for edge sensitive
				LDR r0, =0xE01FC148
				LDR r1, [r0]
				ORR r1, r1, #2  ; EINT1 = Edge Sensitive
				STR r1, [r0]
				
				; Enable Timer0 to interrupt
				LDR r0, =T0MCR			; Load T0MCR address
				LDR r1, [r0]			; Load contents
				ORR r1, r1, #0x18		; Set bits 4:3 to 11 (0x18 == 0001 1000)
				STR r1, [r0]			; Store contents back

				; Enable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				; Bit 6 is for FIQ, Bit 7 is for IRQ
				MRS r0, CPSR
				BIC r0, r0, #0x40     ; Clear bit 6 to enable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0

				LDMFD SP!, {r0-r1, lr} ; Restore registers
				BX lr             	   ; Return
				
				
disable_interrupts
				
				STMFD SP!, {r0-r12, lr}
				
				; Disable Interrupts
				; 1 == disable interrupt
				; 0xFFFFF014 is the interrupt disable register address
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x14] 
				ORR r1, r1, #0x8000 ; Set bit to disable External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt disable
				ORR r1, r1, #0x10	; Timer0 interrupt (used for character movement / screen update)
				STR r1, [r0, #0x14]
				
				; Disable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				MRS r0, CPSR
				ORR r0, r0, #0x40     ; Set bit 6 to disable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				
				
				
set_AT_WALL_based_on_char_position

; AT_TOP_WALL == 1     at top wall, 0 not   position in range: [19,33]
; AT_LEFT_WALL == 1    at left wall, 0 not  (char_position - 1) % 18 == 0
; AT_RIGHT_WALL == 1   at right wall	(char_position + 3) % 18 == 0 
; AT_BOTTOM_WALL == 1  at bottom wall	position in range: [271, 285]
; Example:
;	If char_position == 19, then AT_TOP_WALL == 1 && AT_LEFT_WALL == 1

				STMFD SP!, {r0-r12, lr}
				
				; Reset all AT_WALL_X variables to 0 initially
				LDR r5, =AT_TOP_WALL
				MOV r3, #0
				STRB r3, [r5]
				LDR r5, =AT_LEFT_WALL
				MOV r3, #0
				STRB r3, [r5]
				LDR r5, =AT_RIGHT_WALL
				MOV r3, #0
				STRB r3, [r5]
				LDR r5, =AT_BOTTOM_WALL
				MOV r3, #0
				STRB r3, [r5]
				
				; Load current character position on gameboard & compare with #19
				; which is the upper left corner position
				LDR r4, =char_position
				LDR r2, [r4]
				CMP r2, #19
				BLT check_left_wall_0
				
				; char_position >= 19 here
				CMP r2, #33		; compare to #33, upper right corner
				BGT check_left_wall_0
				
				; We are at top wall here [19,33]
				; Set boolean to 1
				LDR r5, =AT_TOP_WALL
				MOV r3, #1
				STRB r3, [r5]
				
				B check_left_wall_0
				
check_left_wall_0
				; If (char_position - 1) % 18 == 0, then we are on the left wall
				SUB r0, r2, #1	; subtract 1 from char_position
				MOV r1, #18		; Place divisor #18 in r1
				BL div_and_mod	; (char_position - 1) / 18 == r0 R r1
				CMP r1, #0		; Compare remainder of division to #0
				BNE check_bottom_wall_0
				
				; We are at left wall here
				; Set boolean to 1
				LDR r5, =AT_LEFT_WALL
				MOV r3, #1
				STRB r3, [r5]
				
				B check_bottom_wall_0

check_bottom_wall_0
				; "build up" immediate #271 == 0x10F
				; 271 is bottom-left corner of gameboard
				MOV r3, #0x1
				MOV r3, r3, LSL #8
				ADD r3, r3, #0xF
				; Now r3 == 271
				CMP r2, r3		; Compare char_position to 271
				BLT check_right_wall_0
				
				; char_position >= 271 here
				; "build up" immediate #285 == 0x11D
				; 285 is bottom-right corner of gameboard 
				MOV r3, #0x1
				MOV r3, r3, LSL #4
				ADD r3, r3, #0x1
				MOV r3, r3, LSL #4
				ADD r3, r3, #0xD
				; r3 == 285
				CMP r2, r3		; Compare char_position to 285
				BGT check_right_wall_0
				
				; char_position >= 271 && char_position > 285 <==> [271,285]
				; Set boolean to 1
				LDR r5, =AT_BOTTOM_WALL
				MOV r3, #1
				STRB r3, [r5]
				
				B check_right_wall_0
				
check_right_wall_0
				; If (char_position + 3) % 18 == 0, we know we are on the right wall
				ADD r0, r2, #3		; Add 3 to char_position
				MOV r1, #18			; Set divisor to 18
				BL div_and_mod	; (char_position + 3) / 18 == r0 R r1
				CMP r1, #0		; Is remainder = 0 ?
				BNE not_at_wall_0
				
				; We are at right wall here
				; Set boolean to 1
				LDR r5, =AT_RIGHT_WALL
				MOV r3, #1
				STRB r3, [r5]
				
				B not_at_wall_0
				
not_at_wall_0	; We're on no wall, and booleans already all reset to 0, so end function
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				



; sets char_identity based on value in r0
; r0 == 0 <=> ASTERISK_ASCII EQU 0x2A	   '*' symbol ASCII
; r0 == 1 <=> AT_ASCII EQU 0x40		   	   '@' symbol ASCII
; r0 == 2 <=> HASH_ASCII EQU 0x23		   '#' symbol ASCII
; r0 == 3 <=> X_ASCII EQU 0x58             'X' symbol ASCII

set_char_identity
				STMFD SP!, {r1-r12, lr}
				
check_next_char_0
				
				CMP r0, #0		
				BNE check_next_char_1
				; r0 == 0, so set char_identity to 0x2A, ASCII for '*'
				LDR r4, =char_identity
				MOV r1, #0x2A
				STRB r1, [r4]
				
				B exit_set_char_identity
				
check_next_char_1
				CMP r0, #1
				BNE check_next_char_2
				; r0 == 1, so set char_identity to 0x40, ASCII for '@'
				LDR r4, =char_identity
				MOV r1, #0x40
				STRB r1, [r4]
				
				B exit_set_char_identity

check_next_char_2
				CMP r0, #2
				BNE check_next_char_3
				; r0 == 2, so set char_identity to 0x23, ASCII for '#'
				LDR r4, =char_identity
				MOV r1, #0x23
				STRB r1, [r4]
				
				B exit_set_char_identity

check_next_char_3
				CMP r0, #3
				BNE exit_set_char_identity
				; r0 == 3, so set char_identity to 0x58, ASCII for 'X'
				LDR r4, =char_identity
				MOV r1, #0x58
				STRB r1, [r4]
				
				B exit_set_char_identity

exit_set_char_identity

				LDMFD SP!, {r1-r12, lr}
				BX lr             	   ; Return
				
				
				
				
; sets char_direction based on value in r0
; r0 == 0 <=> UP			; UP is -18 (going one position up on game board)
; r0 == 1 <=> DOWN 			; DOWN is +18 (going one position down on game board)
; r0 == 2 <=> LEFT 			; LEFT is -1 (going one position left on game board)
; r0 == 3 <=> RIGHT 		; RIGHT is +1 (going one position right on game board)	

set_char_direction
				STMFD SP!, {r0-r12, lr}
				
check_next_dir_0
				CMP r0, #0
				BNE check_next_dir_1
				; UP
				LDR r4, =char_direction
				MOV r1, #UP
				STR r1, [r4]
				
				B exit_set_char_dir
				
check_next_dir_1
				CMP r0, #1
				BNE check_next_dir_2
				; DOWN
				LDR r4, =char_direction
				MOV r1, #DOWN
				STR r1, [r4]
				
				B exit_set_char_dir

check_next_dir_2
				CMP r0, #2
				BNE check_next_dir_3
				; LEFT
				LDR r4, =char_direction
				MOV r1, #LEFT
				STR r1, [r4]
				
				B exit_set_char_dir

check_next_dir_3
				CMP r0, #3
				BNE exit_set_char_dir
				; RIGHT
				LDR r4, =char_direction
				MOV r1, #RIGHT
				STR r1, [r4]
				
				B exit_set_char_dir

exit_set_char_dir

				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				

; Converts numeric value in SCORE to string and places resulting string in SCORE_STRING
stringify_SCORE
				STMFD SP!, {r0-r12, lr}
				
				LDR r4, =SCORE
				LDR r2, [r4]		; r2 := value from SCORE
				LDR r4, =SCORE_STRING	; r4 := address of SCORE_STRING
				
				; Arguments are r2 and r4
				; r2: value to be converted to string
				; r4: address of string to be stored in memory
				BL make_chars_2
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return

				

FIQ_Handler		STMFD SP!, {r0-r12, lr}   ; Save registers

EINT1			; Check for EINT1 interrupt
				; External Interrupt Flag Register (EXTINT) address
				; EINT0 - EINT3, Pins 0-3
				; 1 == Interrupt Pending
				LDR r0, =0xE01FC140    
				LDR r1, [r0]
				TST r1, #2      ; Note that TST takes the logical AND of r1 and #2, and sets CPSR upper four bits based on result
				BEQ UART0       ; If we have no EINT1 interrupt, goto UART0 to see if the interrupt came from UART0
			
				STMFD SP!, {r0-r12, lr}   ; Save registers, EINT1
					
				; Push button EINT1 Handling Code (We have a push button interrupt here)
				
				; Check if game is paused
				LDR r4, =PAUSED
				LDRB r0, [r4]
				CMP r0, #1
				BEQ continue		; if game IS paused, goto continue to clear interrupt (we don't do anything with push button when paused)
				
				; Get value of T1TC to generate random number for char_identity & char_direction
				LDR r4, =T1TC
				LDR r9, [r4]	; r9 == value of T1TC
				
				; Generate random int for character identity
				; Isolate bits 1:0 of T1TC to generate random int in range [0,3]
				MOV r1, r9, LSL #30
				MOV r1, r1, LSR #30
				MOV r0, r1
				BL set_char_identity
				
				; Generate random int for character direction
				; Isolate bits 3:2 of T1TC to generate random int in range [0,3]
				MOV r1, r9, LSL #28
				MOV r1, r1, LSR #30
				MOV r0, r1
				BL set_char_direction
			
				; Modify gameboard strings to place 'Space' (0x20) at last char_position
				LDR r4, =char_position
				LDR r0, [r4]
				MOV r3, #0x20
				LDR r4, =gb_0		; base address of gameboard strings
				STRB r3, [r4, r0]
				
				; Center new, random character in middle of game board at position 152
				; char_position := 152
				LDR r4, =char_position
				LDR r0, =GB_CENTER
				STR r0, [r4]
				
				; Modify gameboard strings to place new randome char at center of gameboard
				LDR r4, =gb_0		; base address of gameboard strings
				LDR r1, =char_identity
				LDRB r5, [r1]
				STRB r5, [r4, r0]	; r0 is offset to center of gameboard == 152
				
				BL print_gameboard
				
				B continue
						
continue		LDMFD SP!, {r0-r12, lr}   ; Restore registers, EINT1
				
				; Used to clear the EINT1 (push button) interrupt
				ORR r1, r1, #2		; Clear Interrupt
				STR r1, [r0]		; r0 == External Interrupt Flag Register
									; Write a 1 to clear interrupt
				
				B FIQ_Exit
				
UART0			; Check for UART0 interrupt
				; 0 in bit 0 of U0IIR == Interrupt Pending, 1 == no interrupt
				LDR r0, =0xE000C008			; r0 == UART Interrupt Identification Register (U0IIR)
				LDR r1, [r0]
				TST r1, #1					; If 1 in bit 0, z == 0 there is no interrupt in UART0
				BNE TIMER0	

				STMFD SP!, {r0-r12, lr}   ; Save registers, UART0
				
				; UART0 interrupt servicing code here
				
				; Get character entered by user from UART0
				STMFD SP!, {r1-r12, lr}   ; Spill registers
				BL read_character
				LDMFD SP!, {r1-r12, lr}   ; Restore registers
				
				; Check if q_PRESSED == 1 <=> user has requested to EXIT game
				; Don't use r0 here, or will wipe out read_character from above
				LDR r4, =q_PRESSED
				LDRB r1, [r4]
				CMP r1, #1
				BEQ UART0_END	; if q_PRESSED == 1, End
				
				; q_PRESSED == 0 if here, so
				; Check if entered ASCII character is 'q'
				CMP r0, #0x71		
				BEQ q_label	

				; if here, 'q' not entered
				; Check if ENTER_PRESSED == 1
				LDR r4, =ENTER_PRESSED
				LDRB r1, [r4]
				CMP r1, #1
				BEQ enter_already_pressed
				
				; Check if entered ASCII character is 'ENTER' (carriage return)
				CMP r0, #0x0D		
				BEQ enter_label	

				B UART0_END			; Enter has not yet been pressed, so end and wait for Enter again

enter_already_pressed

				; Check is PAUSED == 1
				LDR r4, =PAUSED
				LDRB r1, [r4]
				CMP r1, #1
				BNE game_is_not_paused
				
				; game IS paused if here
				CMP r0, #0x20		; Check if entered ASCII character is 'Space'
				BNE UART0_END
				
				; If here, game is paused, and 'Space' is character entered by user
space_label_paused
				; Toggle PAUSED to 0 (Unpause game)
				LDR r4, =PAUSED
				MOV r1, #0
				STRB r1, [r4]
				
				; Re-Enable Timer0
				LDR r4, =T0TCR
				LDR r1, [r4]
				ORR r1, r1, #0x01
				STR r1, [r4]
				
				B UART0_END
				
game_is_not_paused
				
				CMP r0, #0x20		; Check if entered ASCII character is 'Space'
				BEQ space_label_not_paused
				
				CMP r0, #0x77		; Check if entered ASCII character is 'w'
				BEQ w_label
				
				CMP r0, #0x61		; Check if entered ASCII character is 'a'
				BEQ a_label
				
				CMP r0, #0x73		; Check if entered ASCII character is 's'
				BEQ s_label
				
				CMP r0, #0x64		; Check if entered ASCII character is 'd'
				BEQ d_label
				
				CMP r0, #0x2B		; Check if entered ASCII character is '+'
				BEQ plus_label
				
				CMP r0, #0x2D		; Check if entered ASCII character is '-'
				BEQ minus_label
				
				B UART0_END
				
q_label			LDR r4, =q_PRESSED		
				MOV r1, #1
				STRB r1, [r4]			; Set q_PRESSED to 1
				
				B UART0_END

enter_label
				; ------------------SETUP GAME CODE-------------------
				; Capture Timer1 TC value to use for randomization
				; Randomize character identity, char_position, char_direction
				; Display gameboard
				; Display "Walls encountered: " + SCORE (in ASCII)
				; ENTER_PRESSED := 1
				; Start Timer0
				
				; Get value of T1TC to generate random number for char_identity & char_direction
				; Capture Timer1 TC value to use for randomization
				LDR r4, =T1TC
				LDR r9, [r4]	; r9 == value of T1TC
				
				; Generate random int for character identity
				; Isolate bits 1:0 of T1TC to generate random int in range [0,3]
				MOV r1, r9
				MOV r2, #0xC0
				AND r0, r2, r1
				MOV r0, r0, LSR #6
				;MOV r0, r1
				BL set_char_direction
				
				; Generate random int for character direction
				; Isolate bits 3:2 of T1TC to generate random int in range [0,3]
				MOV r1, r9, LSL #28
				MOV r1, r1, LSR #30
				MOV r0, r1
				BL set_char_identity
				
				; Generate random int for char_position row
				; Isolate bits 5:2 of T1TC to generate random int in range [0,15]
				MOV r1, r9, LSL #26
				MOV r1, r1, LSR #28
				MOV r2, r1 ; store row temporarily
				CMP r2, #15
				MOVEQ r2, #0 ; if r2 == 15, r2 := 0   Now range of r2 is [0,14]
				
				; Generate random int for char_position column
				; Isolate bits 7:4 of T1TC to generate random int in range [0,15]
				MOV r1, r9, LSL #24
				MOV r1, r1, LSR #28
				MOV r3, r1 ; store col temporarily
				CMP r3, #15
				MOVEQ r3, #0 ; if r3 == 15, r3 := 0   Now range of r3 is [0,14]
				
				; Multiply r2 (row) by 18
				MOV r5, r2, LSL #4		; r5 := r2 x 16
				ADD r5, r5, r2
				ADD r5, r5, r2
				
				ADD r2, r3, #19			; r2 := column + 19
				ADD r2, r2, r5			; r2 := (column + 19) + (row x 18) == char_position (random)
				
				LDR r4, =char_identity
				LDRB r10, [r4]			; load char_identity ASCII value
				
				LDR r4, =char_position
				STR r2, [r4]			; store randomly generated char_position 
				
				
				; update AT_WALL_X variables in memory for next Timer0 interrupt
				BL set_AT_WALL_based_on_char_position
				
				; modify gameboard strings to reflect initial random character position
				LDR r4, =gb_0		; base address of gameboard strings
				STRB r10, [r4, r2]	; r2 is offset in memory to where char is initially 
				
				; ENTER_PRESSED := 1
				LDR r4, =ENTER_PRESSED
				MOV r0, #1
				STRB r0, [r4]			; Set ENTER_PRESSED to 1
				
				BL print_gameboard
				
				; Enable / Start Timer0
				LDR r4, =MR1
				LDR r0, =TIMEOUT_PERIOD
				LDR r5, =TIMEOUT_PERIOD_INIT
				STR r5, [r0]
				STR r5, [r4]			; Initialize MR1 value to 9,216,000 ticks == 1 update per half-second
				
				LDR r4, =T0TCR
				LDR r0, [r4]
				ADD r0, r0, #1			; Bit_0 == 1 <=> Enable
				STR r0, [r4]

				B UART0_END

space_label_not_paused

				; Toggle PAUSED to 1
				LDR r4, =PAUSED
				MOV r1, #1
				STRB r1, [r4]

				; Disable Timer0 (STOP TC from counting up while paused)
				LDR r4, =T0TCR
				LDR r1, [r4]
				BIC r1, r1, #0x01
				STR r1, [r4]

				B UART0_END

w_label			LDR r4, =char_direction		
				MOV r1, #UP
				STR r1, [r4]				; set direction to UP

				B UART0_END
				
a_label			LDR r4, =char_direction		
				MOV r1, #LEFT
				STR r1, [r4]				; set direction to LEFT

				B UART0_END

s_label			LDR r4, =char_direction
				MOV r1, #DOWN
				STR r1, [r4]				; set direction to DOWN

				B UART0_END

d_label			LDR r4, =char_direction
				MOV r1, #RIGHT
				STR r1, [r4]				; set direction to RIGHT

				B UART0_END

plus_label		LDR r4, =plus_PRESSED
				MOV r1, #1
				STRB r1, [r4]				; Set plus_PRESSED Boolean to 1, Increasing speed handled on next Timer0 interrupt by checking flag
				
				B UART0_END

minus_label		LDR r4, =minus_PRESSED
				MOV r1, #1
				STRB r1, [r4]				; Set minus_PRESSED Boolean to 1, Decreasing speed handled on next Timer0 interrupt by checking flag
				
				B UART0_END
				
				
UART0_END		LDMFD SP!, {r0-r12, lr}     ; Restore registers, UART0
				B FIQ_Exit
				

TIMER0			; Check for a Timer0 interrupt due to MR1 matching TC
				LDR r4, =T0IR
				LDR r1, [r4]
				TST r1, #2		; If Bit 1 of T0IR is 1, there is an interrupt
				BEQ FIQ_Exit	; If Bit 1 of TOIR is 0, there is NO interrupt, so goto FIQ_Exit (in this case, z==0 in CPSR)
				
				
				STMFD SP!, {r0-r12, lr}   ; Save registers, TIMER0
				
				; Code to service TIMER0 interrupt
				
				; check Boolean to see if plus pressed
				; If so, increase speed
				LDR r4, =plus_PRESSED
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_for_minus_pressed
				
				; plus pressed
				LDR r1, =TIMEOUT_PERIOD
				LDR r5, [r1]			; Contents of TIMEOUT_PERIOD
				MOV r5, r5, LSR #1		; TIMEOUT_PERIOD := TIMEOUT_PERIOD / 2
				STR r5, [r1]			; Store new TIMEROUT_PERIOD value back to memory
				
				LDR r4, =MR1
				STR r5, [r4]			; MR1 is now half the value it had been
				
				LDR r4, =plus_PRESSED
				MOV r0, #0
				STRB r0, [r4]			; Set Boolean plus_PRESSED back to 0
				
				B check_walls_begin
			
check_for_minus_pressed
				LDR r4, =minus_PRESSED	; Check to see if minus_PRESSED is 1
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_walls_begin
				
				; Here, we know minus '-' was pressed
				LDR r1, =TIMEOUT_PERIOD
				LDR r5, [r1]
				MOV r5, r5, LSL #1		; TIMEOUT_PERIOD := TIMEOUT_PERIOD * 2
				STR r5, [r1]			; store new value TIMEOUT_PERIOD back to memory
				
				LDR r4, =MR1
				STR r5, [r4]			; MR1 is now double the value it had been
				
				LDR r4, =minus_PRESSED
				MOV r0, #0
				STRB r0, [r4]			; Set minus_PRESSED Boolean back to 0

				B check_walls_begin
				
; Check to see if character is currently on a wall.
check_walls_begin
				LDR r4, =char_direction
				LDR r2, [r4]	; contents of char_direction
				
check_top_wall_1
				LDR r4, =AT_TOP_WALL
				LDRB r0, [r4]
				CMP r0, #1				; Check if Boolean is 1, if so, at top wall
				BNE check_left_wall_1
				
				; Check if char_direction == UP
				CMP r2, #UP
				BNE check_left_wall_1
				
				; We are at the TOP wall & dir == UP
				; Increment SCORE (SCORE++)
				LDR r4, =SCORE	
				LDR r3, [r4]
				
				ADD r3, r3, #1	; Increment Score
				
				; "build" 1000 == 0x3E8
				MOV r11, #0x3
				MOV r11, r11, LSL #4
				ADD r11, r11, #0xE
				MOV r11, r11, LSL #4
				ADD r11, r11, #0x8
				; If r3 == 1000, r3:=000 (wrap around)
				CMP r3, r11
				MOVEQ r3, #0
				
				STR r3, [r4]		; Store new score back to SCORE in memory
				
				LDR r4, =char_direction
				MOV r3, #DOWN
				STR r3, [r4]		; Reverse direction
				
				B proceed_as_usual
							
check_left_wall_1	; Are we at the left wall?
				LDR r4, =AT_LEFT_WALL
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_right_wall_1
				
				; Check if char_direction == LEFT
				CMP r2, #LEFT
				BNE check_right_wall_1
				
				; We are at the LEFT wall here & dir == LEFT
				; Increment SCORE (SCORE++)
				LDR r4, =SCORE	
				LDR r3, [r4]
				
				ADD r3, r3, #1
				
				; "build" 1000 == 0x3E8
				MOV r11, #0x3
				MOV r11, r11, LSL #4
				ADD r11, r11, #0xE
				MOV r11, r11, LSL #4
				ADD r11, r11, #0x8
				; If r3 == 1000, r3:=000 (wrap around)
				CMP r3, r11
				MOVEQ r3, #0
				
				STR r3, [r4]
				
				LDR r4, =char_direction
				MOV r3, #RIGHT
				STR r3, [r4]			; Reverse direction
				
				B proceed_as_usual

check_right_wall_1 ; Are we at the right wall?
				LDR r4, =AT_RIGHT_WALL
				LDRB r0, [r4]
				CMP r0, #1
				BNE check_bottom_wall_1
				
				; Check if char_direction == RIGHT
				CMP r2, #RIGHT
				BNE check_bottom_wall_1
				
				; We are at the RIGHT wall here & dir == RIGHT
				; Increment SCORE (SCORE++)
				LDR r4, =SCORE	
				LDR r3, [r4]
				
				ADD r3, r3, #1
				
				; "build" 1000 == 0x3E8
				MOV r11, #0x3
				MOV r11, r11, LSL #4
				ADD r11, r11, #0xE
				MOV r11, r11, LSL #4
				ADD r11, r11, #0x8
				; If r3 == 1000, r3:=000 (wrap around)
				CMP r3, r11
				MOVEQ r3, #0
				
				STR r3, [r4]
				
				LDR r4, =char_direction
				MOV r3, #LEFT
				STR r3, [r4]		; Reverse direction
				
				B proceed_as_usual
				
check_bottom_wall_1 ; Are we at the BOTTOM wall?
				LDR r4, =AT_BOTTOM_WALL
				LDRB r0, [r4]
				CMP r0, #1
				BNE proceed_as_usual
				
				; Check if char_direction == DOWN
				CMP r2, #DOWN
				BNE proceed_as_usual
				
				; We are at the BOTTOM wall here & dir == DOWN
				; Increment SCORE (SCORE++)
				LDR r4, =SCORE	
				LDR r3, [r4]
				
				ADD r3, r3, #1
				
				; "build" 1000 == 0x3E8
				MOV r11, #0x3
				MOV r11, r11, LSL #4
				ADD r11, r11, #0xE
				MOV r11, r11, LSL #4
				ADD r11, r11, #0x8
				; If r3 == 1000, r3:=000 (wrap around)
				CMP r3, r11
				MOVEQ r3, #0
				
				STR r3, [r4]
				
				LDR r4, =char_direction
				LDR r3, =UP
				STR r3, [r4]		; Reverse direction
				
				B proceed_as_usual
					
proceed_as_usual				
				; Update char position based on current direction of movement

				LDR r4, =char_position
				LDR r1, [r4]
				MOV r3, r1 		; temporarily store old char_position
				
				LDR r4, =char_direction
				LDR r2, [r4]	; contents of char_direction
				
				ADD r1, r1, r2		; char_position := char_position + char_direction
				LDR r4, =char_position
				STR r1, [r4]		; update char_position in memory
				
				; update AT_WALL_X variables in memory for next Timer0 interrupt
				BL set_AT_WALL_based_on_char_position

continue_Timer0
				
				; old char_position == r3
				; new char_position == r1
				; update gameboard strings to reflect player movement
				
				LDR r4, =gb_0		; base address of gameboard strings
				MOV r5, #0x20		; 0x20 == 'Space'
				STRB r5, [r4, r3]	; r3 is offset in memory to where char used to be ; store 'Space' at old char position
				
				LDR r2, =char_identity
				LDRB r5, [r2]
				STRB r5, [r4, r1]	; change gameboard strings with new character position
				
				BL print_gameboard
				
				LDMFD SP!, {r0-r12, lr}   ; Restore registers, TIMER0
				
				; Used to clear the TIMER0 interrupt
				; We clear interrupt by setting bit 1 to 1
				LDR r4, =T0IR
				LDR r1, [r4]
				ORR r1, r1, #2
				STR r1, [r4]
				
				B FIQ_Exit
				
FIQ_Exit		LDMFD SP!, {r0-r12, lr}
				SUBS pc, lr, #4

	END